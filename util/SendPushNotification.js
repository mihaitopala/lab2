import PushNotification from 'react-native-push-notification';
import moment from 'moment';

const sendPushNotification = (date, title, id) => {
  const options = {
    id,
    title: 'Remind to do task',
    date: new Date(date),
    message: title
  }

  return PushNotification.localNotificationSchedule(options);
};

export const cancelNotification = data => {
  const { id, remind_time, endDate, title } = data;
  const newDate = moment(endDate).subtract(remind_time, 'minutes');
  
  PushNotification.cancelLocalNotifications({ id: String(id) });
  sendPushNotification(newDate, title, id);
}

export default sendPushNotification;