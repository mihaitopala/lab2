import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import MainScreen from './screens/MainScreen';
import TodoCreateScreen from './screens/TodoCreateScreen';
import TodoEditScreen from './screens/TodoEditScreen';
import TodosListScreen from './screens/TodosListScreen';

const MainNavigator = createStackNavigator(
  {
    Main: { screen: MainScreen },
    TodosList: { screen: TodosListScreen },
    CreateTodo: { screen: TodoCreateScreen },
    EditTodo: { screen: TodoEditScreen },
  },
  {
    headerLayoutPreset: 'center'
  }
);

const App = createAppContainer(MainNavigator);

export default App;
