import db from '../Database';

const getAllTodos = () => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      txn.executeSql(
        `SELECT * FROM todos`,
        [],
        (tx, results) => {
          const len = results.rows.length;
            const todos = [];

            for (let i = 0; i < len; ++i) {
              todos.push(results.rows.item(i));
            } 

            const final = todos.reduce((obj, item) => {
              return {
                ...obj,
                [item['startDate']]: item
              }
            }, {});

            resolve(final);
        },
      );
    });
  })
};

const getTodosByCreatedDate = date => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      txn.executeSql(
        `SELECT * FROM todos WHERE startDate LIKE '%${date}%'`,
        [],
        (tx, results) => {
          const len = results.rows.length;
          const todos = [];

          if (len > 0) {

            for (let i = 0; i < len; ++i) {
              todos.push(results.rows.item(i));
            } 

            resolve(todos);
          }

          resolve([])
        },
      );
    });
  });
};

const deleteTodo = id => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      txn.executeSql('DELETE FROM todos where id=?', [id], (tx, results) => {
        if (results.rowsAffected > 0) {
          resolve({ success: true })
        } else {
          reject(new Error('Failed to delete item'))
        }
      });
    });
  })
};

const searchTodo = query => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      txn.executeSql(
        `SELECT * FROM todos WHERE title LIKE '%${query}%' OR description LIKE '%${query}%'`,
        [],
        (tx, results) => {
          const len = results.rows.length;
          if (len > 0) {
            const todos = [];

            for (let i = 0; i < len; ++i) {
              todos.push(results.rows.item(i));
            } 

            resolve(todos);
          } else {
            reject(new Error('No data found!'))
          }
        },
      );
    });
  })
};

const createTodo = data => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      txn.executeSql(
        'INSERT into todos (title, description, startDate, endDate, remind_time) VALUES (?, ?, ?, ?, ?)',
        [data.title, data.description, data.startDate, data.endDate, data.remind_time],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            resolve({ 
              id: results.insertId, 
              success: true 
            })
          } else {
            reject(new Error('Failed to create new todo!'))
          }
        },
      );
    });
  })
};

const updateTodo = (data) => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      txn.executeSql(
        'UPDATE todos SET title=?, description=?, startDate=?, endDate=?, remind_time=? WHERE id=?',
        [data.title, data.description, data.startDate, data.endDate, data.remind_time, data.id],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            resolve({ success: true })
          } else {
            reject(new Error('Failed to update todo!'))
          }
        },
      );
    });
  })
};

module.exports = {
  getTodosByCreatedDate,
  deleteTodo,
  searchTodo,
  createTodo,
  updateTodo,
  getAllTodos,
};
