import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  View,
} from 'react-native';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import Title from '../components/header/Title';
import Search from '../components/Search';
import Calendar from '../components/calendar/Calendar';
import { getAllTodos } from '../services/controllers/TodoController';

class MainScreen extends Component {
  static navigationOptions = () => {
    return {
      headerTitle: () => <Title title="Todos Calendar" />,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      todos: {},
      currentMonth: moment().format('YYYY-MM')
    };
  }

  componentDidMount() {
    const { currentMonth } = this.state;
    this.getTodos(currentMonth)
  }

  getTodos = async () => {
    try {
      const data = await getAllTodos();
      if (data && Object.keys(data).length) {
        this.setState({ todos: data })
      }

    } catch (error) {
      Alert.alert('Error', error.message, [
        {
          text: 'Ok'
        },
      ]);
    }

  }

  render() {
    const { todos } = this.state;
    const {navigation} = this.props;

    if (!todos) return null;

    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.sectionContainer}>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <Search navigation={navigation} />
            <Calendar 
              data={Object.keys(todos)}
              handleMonthChange={this.handleMonthChange}
              navigation={navigation} 
            />
            <View style={styles.bottomBlock}>
              <Icon.Button
                style={styles.addButton}
                name="plus"
                onPress={() => navigation.push('CreateTodo')}>
                Add todo
              </Icon.Button>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

export default MainScreen;

const styles = StyleSheet.create({
  scrollView: {
    marginHorizontal: 0,
  },
  sectionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  bottomBlock: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  addButton: {
    backgroundColor: '#3b5998',
    paddingLeft: 50,
    paddingRight: 50,
    height: 45,
  },
});
