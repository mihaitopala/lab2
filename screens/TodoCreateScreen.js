import React, {Component} from 'react';
import {View, StyleSheet, ActivityIndicator, Alert} from 'react-native';
import moment from 'moment';
import Title from '../components/header/Title';

import TodoForm from '../components/forms/TodoForm';

import { createTodo } from '../services/controllers/TodoController';
import sendPushNotification from '../util/SendPushNotification';

class TodoCreateScreen extends Component {
  state = {
    loading: false
  }

  static navigationOptions = () => {
    return {
      headerTitle: () => <Title title="Create todo" />,
    };
  };

  handleSubmit = async data => {
    const { navigation } = this.props;

    try {
      this.setState({ loading: true });
      const response = await createTodo(data);

      if (response && response.success) {
        this.setState({ loading: false });

        if (data.remind_time) {
          const newDate = moment(data.endDate).subtract(data.remind_time, 'minutes');
          sendPushNotification(newDate, data.title, response.id);
        }

        navigation.push('Main');
      }

    } catch (error) {
      Alert.alert('Error', error.message, [
        {
          text: 'Ok'
        },
      ]);
    }
  };


  render() {
    const { loading } = this.state;

    if (loading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <TodoForm onSubmit={this.handleSubmit} />
      </View>
    );
  }
}

export default TodoCreateScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 15,
  }
});
