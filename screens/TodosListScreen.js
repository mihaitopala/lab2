import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  Button,
  Alert
} from 'react-native';
import Title from '../components/header/Title';
import { getTodosByCreatedDate, deleteTodo } from '../services/controllers/TodoController';

class TodosListScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      loading: false,
    };
  }

  static navigationOptions = () => {
    return {
      headerTitle: () => <Title title="Todos list" />,
    };
  };

  componentDidMount() {
    const {navigation} = this.props;

    const date = navigation.getParam('date');
    const data = navigation.getParam('data');

    if (!data && date && date.dateString ) {
      // get from db todo
      this.getTodosByDay(date.dateString);
    } else {
      this.setState({ data })
    }
  }

  handleDelete = async id => {
    try {
      this.setState({ loading: true });
      const response = await deleteTodo(id);
      if (response) {
        const { navigation } = this.props;

        const newArr = this.state.data.filter(item => item.id !== id);
        this.setState({
          data: newArr,
          loading: false
        })

        if (newArr.length === 0) {
          navigation.push('Main');
        }
      }
    } catch (error) {
      this.setState({ loading: false });
      Alert.alert('Error', error.message, [
        {
          text: 'Ok'
        },
      ]);
    }
  };

  getTodosByDay = async (data) => {
    try {
      this.setState({ loading: true })
      const result = await getTodosByCreatedDate(data);
      
      if (result && result.length) {
        this.setState({ data: result, loading: false });
      } else {
        this.setState({ loading: false })
      }

    } catch (error) {
      this.setState({ loading: false })
      Alert.alert('Error', error.message, [
        {
          text: 'Ok'
        },
      ]);
    }
  }

  render() {
    const {loading, data} = this.state;
    const {navigation} = this.props;

    console.log('result data', data)
    if (loading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
      <View style={[styles.container]}>
        <FlatList
          style={styles.list}
          data={data}
          renderItem={({item, index}) => (
            <>
              <View style={styles.subContainer}>
                <View style={styles.listItemCont}>
                  <Text style={styles.listItem}>Title: {item.title}</Text>
                </View>
                <View style={styles.listItemCont}>
                  <Text style={styles.listItem}>Description: {item.description}</Text>
                </View>
                <View style={styles.listItemCont}>
                  <Text style={styles.listItem}>Start at: {item.startDate}</Text>
                </View>
                <View style={styles.listItemCont}>
                  <Text style={styles.listItem}>End at: {item.endDate}</Text>
                </View>
                <View style={styles.buttons}>
                  <Button title="Delete" style={styles.deleteBtn} onPress={() => this.handleDelete(item.id)} />
                  <Button title="Edit" onPress={() => navigation.push('EditTodo', {item})} />
                </View>
              </View>
              <View style={styles.hr} />
            </>
          )}
        />
      </View>
    );
  }
}

export default TodosListScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    padding: 10,
    paddingTop: 20,
  },
  list: {
    width: '100%',
  },
  listItem: {
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 18,
  },
  hr: {
    height: 1,
    backgroundColor: 'gray',
    marginTop: 5
  },
  listItemCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  buttons: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10
  },
  deleteBtn: {
    backgroundColor: 'red',
    }
});
