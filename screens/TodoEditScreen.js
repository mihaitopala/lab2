import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Title from '../components/header/Title';

import TodoForm from '../components/forms/TodoForm';
import { updateTodo } from '../services/controllers/TodoController';
import { cancelNotification } from '../util/SendPushNotification';

class TodoEditScreen extends Component {
  static navigationOptions = () => {
    return {
      headerTitle: () => <Title title="Edit todo" />,
    };
  };

  handleSubmit = async data => {
    const { navigation } = this.props;

    try {
      const response = await updateTodo(data);
      cancelNotification(data);
      if (response && response.success) {
        navigation.push('Main');
      }
    } catch (error) {
      Alert.alert('Error', error.message, [
        {
          text: 'Ok'
        },
      ]);
    }
  };

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('item');

    return (
      <View style={{flex: 1}}>
        <TodoForm onSubmit={this.handleSubmit} data={data} />
      </View>
    );
  }
}

export default TodoEditScreen;

const styles = StyleSheet.create({});
