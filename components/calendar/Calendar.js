import React, {Component} from 'react';
import {Calendar} from 'react-native-calendars';
import {View, StyleSheet} from 'react-native';
import moment from 'moment';

const color = '#3b5998';

class CustomCalendar extends Component {
  constructor(props) {
    super(props);
  }

  
  render() {
    const { navigation, data } = this.props;

    const newData = data.reduce((obj, item) => {
      return {
        ...obj,
        [item.split(' ')[0]]: {selected: true, selectedColor: color}
      }
    }, {});

    return (
      <View style={styles.container}>
        <Calendar
          markedDates={newData}
          // Initially visible month. Default = Date()
          current={new Date()}
          // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
          minDate={moment().startOf('month').format('YYYY-MM-DD')}
          // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
          maxDate={moment().endOf('month').format('YYYY-MM-DD')}
          // Handler which gets executed on day press. Default = undefined
          onDayPress={day => {
            navigation.push('TodosList', { date: day })
          }}
          // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
          monthFormat={'yyyy MMMM'}
          // Handler which gets executed when visible month changes in calendar. Default = undefined
          onMonthChange={month => {
            console.log('month changed', month);
          }}
          // Hide month navigation arrows. Default = false
          hideArrows={false}
          // Do not show days of other months in month page. Default = false
          hideExtraDays={true}
          // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
          // day from another month that is visible in calendar page. Default = false
          disableMonthChange={true}
          // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
          firstDay={1}
          // Hide day names. Default = false
          hideDayNames={true}
          // Show week numbers to the left. Default = false
          showWeekNumbers={true}
          // Handler which gets executed when press arrow icon left. It receive a callback can go back month
          onPressArrowLeft={substractMonth => substractMonth()}
          // Handler which gets executed when press arrow icon left. It receive a callback can go next month
          onPressArrowRight={addMonth => addMonth()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#D1D5DA'
  }
});
export default CustomCalendar;
