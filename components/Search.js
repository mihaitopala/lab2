import React, {useState} from 'react';
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert
} from 'react-native';
import { searchTodo } from '../services/controllers/TodoController';


const Search = ({ navigation }) => {

  const [searchText, setSearchText] = useState('');

  handleClick = async () => {
    if (searchText.length) {
      try {
        const data = await searchTodo(searchText);

        if (data && data.length) {
          navigation.push('TodosList', { data })
        }

      } catch (error) {
        Alert.alert('Error', error.message, [
          {
            text: 'Ok'
          },
        ]);
      }
    }
  };

  return (
    <View>
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          value={searchText}
          onChangeText={text => setSearchText(text)}
          placeholder="Search by title or description..."
        />
        <TouchableOpacity style={styles.button} onPress={handleClick}>
          <Text style={styles.text}> Search </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Search;

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    marginBottom: 15,
    fontFamily: 'Roboto-Medium',
    fontSize: 18
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
    paddingBottom: 30
  },
  text: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingBottom: 5,
    color: '#fff'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#3b5998',
    color: '#fff',
    padding: 10,
    borderRadius: 24,
    marginLeft: 10
  },
  input: {
    height: 40,
    borderColor: '#dfe1e5',
    borderWidth: 1,
    width: 230,
    borderRadius: 10
  },
});