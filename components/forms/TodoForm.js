import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  View,
  TextInput,
  Text,
  Picker,
} from 'react-native';

import DatePicker from 'react-native-datepicker';

import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

const values = [1, 5, 10, 30, 60];

class TodoForm extends Component {
  constructor(props) {
    super(props);
    const { data } = props;

    this.state = data || {
      title: '',
      description: '',
      startDate: moment(),
      endDate: moment(),
      remind_time: 5,
      errors: {},
    };
  }
  
  updateTextInput = (value, key) => {
    this.setState({
      [key]: value,
    });
  };

  saveData = () => {
    delete this.state.errors;

    if (this.validateData()) {
      this.props.onSubmit(this.state);
    }
  };

  validateData = () => {
    const errors = {};

    if (!this.state.title.length) {
      errors.title = {
        message: 'Title is required',
      };
    }

    if (!this.state.description.length) {
      errors.description = {
        message: 'Description is required',
      };
    }

    if (!this.state.startDate.length) {
      errors.startDate = {
        message: 'Start date is required',
      };
    }

    if (!this.state.endDate.length) {
      errors.endDate = {
        message: 'End date is required',
      };
    }

    if (this.state.startDate > this.state.endDate) {
      errors.startDate = {
        message: "Start date can't be greater than end date",
      };
    }

    this.setState({errors});

    if (Object.keys(errors).length) {
      return false;
    }

    return true;
  };

  render() {
    const {loading} = this.props;
    const {errors} = this.state;

    const options = values.map(item => (
      <Picker.Item
        label={`Remind me ${item} minutes in advance`}
        value={item}
      />
    ));

    if (loading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
      <ScrollView style={styles.container}>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Title'}
            value={this.state.title}
            onChangeText={text => this.updateTextInput(text, 'title')}
          />
        </View>
        {errors && errors['title'] && (
          <Text style={{color: 'red'}}>{errors['title'].message}</Text>
        )}
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Description'}
            value={this.state.description}
            onChangeText={text => this.updateTextInput(text, 'description')}
          />
        </View>
        {errors && errors['description'] && (
          <Text style={{color: 'red'}}>{errors['description'].message}</Text>
        )}
        <View style={styles.subContainer}>
          <DatePicker
            date={undefined}
            mode="datetime"
            placeholder="Select date"
            format="YYYY-MM-DD HH:mm"
            minDate={moment()}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                display: 'none',
              },
            }}
            TouchableComponent={props => (
              <TextInput
                placeholder={'Start date'}
                onFocus={() => props.onPress()}
                value={this.state.startDate}
              />
            )}
            onDateChange={value => this.updateTextInput(value, 'startDate')}
          />
        </View>
        {errors && errors['startDate'] && (
          <Text style={{color: 'red'}}>{errors['startDate'].message}</Text>
        )}
        <View style={styles.subContainer}>
          <DatePicker
            date={undefined}
            mode="datetime"
            placeholder="Select date"
            format="YYYY-MM-DD HH:mm"
            minDate={moment()}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                display: 'none',
              },
            }}
            TouchableComponent={props => (
              <TextInput
                placeholder={'End date'}
                onFocus={() => props.onPress()}
                value={this.state.endDate}
              />
            )}
            onDateChange={value => this.updateTextInput(value, 'endDate')}
          />
        </View>
        {errors && errors['endDate'] && (
          <Text style={{color: 'red'}}>{errors['endDate'].message}</Text>
        )}
        <View style={styles.subContainer}>
          <Picker
            selectedValue={this.state.remind_time}
            style={{height: 50}}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({remind_time: itemValue})
            }>
            {options}
          </Picker>
        </View>
        <View style={styles.buttonBlock}>
          <Icon.Button
            name="save"
            style={styles.button}
            onPress={() => this.saveData()}>
            Save
          </Icon.Button>
        </View>
      </ScrollView>
    );
  }
}

export default TodoForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  subContainer: {
    flex: 1,
    marginBottom: 20,
    padding: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#CCCCCC',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonBlock: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  button: {
    backgroundColor: '#3b5998',
    paddingLeft: 50,
    paddingRight: 50,
    height: 45,
  },
});
